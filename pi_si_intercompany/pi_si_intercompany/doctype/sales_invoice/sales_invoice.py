# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
import json
from frappe import msgprint, _
from frappe import _
from frappe.utils import cint, cstr, date_diff, flt, formatdate, getdate, get_link_to_form, \
	comma_or, get_fullname
from frappe.model.document import Document
from frappe.exceptions import ValidationError
import decimal
from frappe.utils import today, flt, cint, fmt_money, formatdate, getdate, add_days, add_months, get_last_day



@frappe.whitelist()
def intercompany_sales_invoice(doc,method):
	customer =  frappe.get_doc ( "Customer" , doc.customer)
	print "on submit"
	print "doc.reversed_purchase_invoice", doc.reversed_purchase_invoice
	print "doc.is_return", doc.is_return
	items = []
	taxes_list = []
	
	if customer.customer_group == 'Intercompany' and (doc.reversed_purchase_invoice == None or doc.reversed_purchase_invoice == '') and doc.is_return == 0:
		print "intercompany"
		company = customer.company

		companyabbr = frappe.db.sql("""SELECT abbr,cost_center,stock_received_but_not_billed from tabCompany where name =%s""",(company))
		if not companyabbr:
			frappe.throw(_("Company {0} Abbr not found").format(company))
		supplier = frappe.db.sql("""SELECT t1.name from tabSupplier t1 where t1.company =%s and t1.supplier_type = "Intercompany"  """,(doc.company))
		supplierabbr = doc.companyabbr

		
		if not supplier:
			frappe.throw(_("Company {0} not found in Supplier list.").format(doc.company))

		for item in doc.items:
			# account = item.income_account
			cost_center = item.cost_center

			# if doc.update_stock == 1:
				
			# 	warehouse_search = frappe.db.sql("""SELECT warehouse from `tabDefault Warehouses`  where  parent=%s and company = %s""",(item.item_code,company))					
			# 	if not warehouse_search:
			# 		frappe.throw(_("Warehouse for {0} not found in Item Master for {1}.").format(company,item.item_code))
			# 	warehouse = warehouse_search[0][0]
			# 	items.append({"item_code": item.item_code, "qty": item.qty, "expense_account": warehouse, "cost_center": companyabbr[0][1], "rate":item.rate, "warehouse": warehouse})
			# else:
			items.append({"item_code": item.item_code, "qty": item.qty, "cost_center": companyabbr[0][1], "rate":item.rate})
		taxes_and_charges = ''
		if len(doc.taxes)>0:
			default_tax =  frappe.db.sql("""SELECT name from `tabPurchase Taxes and Charges Template` where company =%s and is_default = 1""",(company))
			if default_tax:
				taxes_and_charges = default_tax[0][0]
				taxes = frappe.get_doc ( "Purchase Taxes and Charges Template" , default_tax[0][0])
				if len(taxes.taxes)> 0:
					for tax in taxes.taxes:
						taxes_list.append(tax)


				# account_head = tax.account_head
				# cost_center = tax.cost_center
				# taxes_list.append({"category":"Total","charge_type": tax.charge_type, "rate": tax.rate, "description": tax.description,"account_head": "GST Input - " + companyabbr[0][0],"cost_center":companyabbr[0][1] })

		pi = frappe.get_doc({"doctype": "Purchase Invoice",
			"title": supplier[0][0],
			"supplier": supplier[0][0],
			"bill_no": doc.name,
			"ignore_princing_rule": 1,
			"update_stock": doc.update_stock,
			"items": items,
			"company": company,
			"currency":doc.currency,
			"companyabbr": companyabbr[0][0],
			"posting_date": doc.posting_date,
			"letter_head":"Earth ID Design Letter Head",
			"taxes_and_charges": taxes_and_charges,
			"taxes": taxes_list,
			"reversed_sales_invoice":doc.name})
		pi.flags.ignore_permissions = True
		pi.save()
		frappe.db.commit()
		doc.reversed_purchase_invoice = pi.name
		doc.save()


@frappe.whitelist()
def intercompany_sales_invoice_cancel(doc,method):
	print "reversed",doc.reversed_purchase_invoice
	if doc.reversed_purchase_invoice != None and doc.reversed_purchase_invoice != '':
		print "read here"
		pi = frappe.get_doc ( "Purchase Invoice" , doc.reversed_purchase_invoice)
		pi.reversed_sales_invoice=None
		pi.save()
		doc.reversed_purchase_invoice = None