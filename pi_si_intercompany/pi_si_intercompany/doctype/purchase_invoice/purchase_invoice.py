# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
import json
from frappe import msgprint, _
from frappe import _
from frappe.utils import cint, cstr, date_diff, flt, formatdate, getdate, get_link_to_form, \
	comma_or, get_fullname
from frappe.model.document import Document
from frappe.exceptions import ValidationError
import decimal


@frappe.whitelist()
def intercompany_purchase_invoice(doc,method):
	supplier =  frappe.get_doc ( "Supplier" , doc.supplier)
	items = []
	taxes_list = []
	print "here"
	
	if supplier.supplier_type == 'Intercompany' and (doc.reversed_sales_invoice ==None or doc.reversed_sales_invoice == '') and doc.is_return == 0:
		company = supplier.company
		companyabbr = frappe.db.sql("""SELECT abbr,default_income_account,default_expense_account,cost_center from tabCompany where name =%s""",(company))
		customer = frappe.db.sql("""SELECT t1.name,t1.credit_days from tabCustomer t1 where t1.company =%s and t1.customer_group ="Intercompany" """,(doc.company))
		if customer[0][1] == None or customer[0][1] == '':
			credit_days = 0
		else:
			credit_days = customer[0][1]
		

		customerabbr = doc.companyabbr
		

		if not customer:
			frappe.throw(_("Company {0} not found in Customer list.").format(doc.company))

		for item in doc.items:


			# warehouse = frappe.db.sql("""SELECT warehouse from `tabDefault Warehouses`  where  parent=%s and company = %s""",(item.item_code,company))
			# if not warehouse:
			# 	frappe.throw(_("Warehouse for {0} not found in Item Master for {1}.").format(company,item.item_code))
			account = item.expense_account
			cost_center = item.cost_center
			is_stock_item = frappe.db.sql("""SELECT is_stock_item from `tabItem` where name=%s""",(item.item_code))
			# if doc.update_stock == 1:
			# 	warehouse = frappe.db.sql("""SELECT warehouse from `tabDefault Warehouses`  where  parent=%s and company = %s""",(item.item_code,company))
			# 	if not warehouse:
			# 		frappe.throw(_("Warehouse for {0} not found in Item Master for {1}.").format(company,item.item_code))

			# 	items.append({"item_code": item.item_code, "qty": item.qty, "income_account": warehouse[0][0], "expense_account":companyabbr[0][2],
			# 	"rate":item.rate ,"cost_center": companyabbr[0][3], "warehouse": warehouse[0][0]})
			# else:
				# inc_acc = frappe.db.sql("""SELECT default_income_account from `tabDefault Income Accounts` where parent=%s and company = %s""",(item.item_code,company))
				# if not inc_acc:
				# 	frappe.throw(_("Default Income Account for {0} not found in Item Master for {1}.").format(company,item.item_code))
			items.append({"item_code": item.item_code, "qty": item.qty, "income_account": companyabbr[0][1],
			"rate":item.rate ,"cost_center": companyabbr[0][3]})

			# else:
			# 	frappe.throw(_("Warehouse for {0} not found in Item Master for {1}.").format(company,item.item_code))
			# 	items.append({"item_code": item.item_code, "qty": item.qty, "income_head": account.replace(customerabbr,companyabbr[0][0]),
			# 	"rate":item.rate ,"cost_center": cost_center.replace(customerabbr,companyabbr[0][0])})
		if len(doc.taxes) > 0:
			default_tax =  frappe.db.sql("""SELECT name from `tabSales Taxes and Charges Template` where company =%s and is_default = 1""",(company))
			if default_tax:
				taxes = frappe.get_doc ( "Sales Taxes and Charges Template" , default_tax[0][0])
				if len(taxes.taxes)> 0:
					for tax in taxes.taxes:
							taxes_list.append(tax)



		si = frappe.get_doc({"doctype": "Sales Invoice",
			"customer": customer[0][0],
			"items": items,
			"title":customer[0][0],
			"company": company,
			"update_stock": doc.update_stock,
			"companyabbr": companyabbr[0][0],
			"posting_date": doc.posting_date,
			"currency":doc.currency,
			"letter_head":"Earth ID Design Letter Head",
			"taxes": taxes_list,
			"credit_days":credit_days,
			"reversed_purchase_invoice":doc.name})
		si.flags.ignore_permissions = True
		si.save()
		frappe.db.commit()
		doc.reversed_sales_invoice = si.name
		doc.save()
		# print "si.name", si.name

@frappe.whitelist()
def intercompany_purchase_invoice_cancel(doc,method):
	if doc.reversed_sales_invoice != None and doc.reversed_sales_invoice != '':
		si = frappe.get_doc ( "Sales Invoice" , doc.reversed_sales_invoice)
		si.reversed_purchase_invoice=None
		si.save()
		doc.reversed_sales_invoice = None