from __future__ import unicode_literals
import frappe

def execute():
	if not frappe.db.sql("Select name from `tabCustom Field` where label = 'Reversal Purchase Invoice'"):
		reversed_pi = frappe.get_doc({
			"doctype":"Custom Field",
			"dt":"Sales Invoice",
			"label":"Reversal Purchase Invoice",
			"fieldtype":"Link",
			"options":"Purchase Invoice",
			"insert_after":"More Information",
			"read_only":1,
			"no_copy":1,
			"allow_on_submit":1
			})
		reversed_pi.save()

	if not frappe.db.sql("Select name from `tabCustom Field` where label = 'Reversal Sales Invoice'"):

		reversed_si = frappe.get_doc({
			"doctype":"Custom Field",
			"dt":"Purchase Invoice",
			"label":"Reversal Sales Invoice",
			"fieldtype":"Link",
			"options":"Sales Invoice",
			"insert_after":"More Information",
			"read_only":1,
			"no_copy":1,
			"allow_on_submit":1
			})
		reversed_si.save()

	if not frappe.db.sql("Select name from `tabCustom Field` where options = 'Company' and  dt='Supplier'"):

		company1 = frappe.get_doc({
			"doctype":"Custom Field",
			"dt":"Supplier",
			"label":"Company",
			"fieldtype":"Link",
			"options":"Company",
			"insert_after":"Country"
			})
		company1.save()
	if not frappe.db.sql("Select name from `tabCustom Field` where options = 'Company' and  dt='Customer'"):
		company2 = frappe.get_doc({
			"doctype":"Custom Field",
			"dt":"Customer",
			"label":"Company",
			"fieldtype":"Link",
			"options":"Company",
			"insert_after":"Status"
			})
		company2.save()

	if not frappe.db.sql("Select name from `tabSupplier Type` where supplier_type = 'Intercompany'"):

		supplier_type = frappe.get_doc({
			"doctype":"Supplier Type",
			"":"Intercompany"
			})

		supplier_type.save()

	if not frappe.db.sql("Select name from `tabCustomer Group` where customer_group_name = 'Intercompany'"):
		customer_group = frappe.get_doc({
			"doctype": "Customer Group",
			"customer_group_name":"Intercompany",
			"parent_customer_group":"All Customer Groups"
			})
		customer_group.save()