# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "pi_si_intercompany"
app_title = "pi_si_intercompany"
app_publisher = "abutalha"
app_description = "Purchase/Sales Invoice Intercompany"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "atalha@cloude8.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/pi_si_intercompany/css/pi_si_intercompany.css"
# app_include_js = "/assets/pi_si_intercompany/js/pi_si_intercompany.js"

# include js, css files in header of web template
# web_include_css = "/assets/pi_si_intercompany/css/pi_si_intercompany.css"
# web_include_js = "/assets/pi_si_intercompany/js/pi_si_intercompany.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "pi_si_intercompany.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "pi_si_intercompany.install.before_install"
# after_install = "pi_si_intercompany.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "pi_si_intercompany.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }
doc_events = {
	"Sales Invoice" : {
		"on_submit" : "pi_si_intercompany.pi_si_intercompany.doctype.sales_invoice.sales_invoice.intercompany_sales_invoice",
		"on_cancel" : "pi_si_intercompany.pi_si_intercompany.doctype.sales_invoice.sales_invoice.intercompany_sales_invoice_cancel"
	},
	"Purchase Invoice" : {
		"on_submit" : "pi_si_intercompany.pi_si_intercompany.doctype.purchase_invoice.purchase_invoice.intercompany_purchase_invoice",
		"on_cancel" : "pi_si_intercompany.pi_si_intercompany.doctype.purchase_invoice.purchase_invoice.intercompany_purchase_invoice_cancel"
	}

}
# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"pi_si_intercompany.tasks.all"
# 	],
# 	"daily": [
# 		"pi_si_intercompany.tasks.daily"
# 	],
# 	"hourly": [
# 		"pi_si_intercompany.tasks.hourly"
# 	],
# 	"weekly": [
# 		"pi_si_intercompany.tasks.weekly"
# 	]
# 	"monthly": [
# 		"pi_si_intercompany.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "pi_si_intercompany.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "pi_si_intercompany.event.get_events"
# }

